import { QueryParser } from "@brownpapertickets/surya-gql-data"

/**
 * Transcoder from QueryParser syntax tree formats to
 * postgreSql WHERE and ORDER BY formats.
 */
export class PGSqlQuery {
  constructor(container, type, typeMap, config) {
    this.log = container.log
    this.type = type // the root Type object

    this.typeMap = typeMap // map object of all the typeSpecs for all objects incase of potential nested reference.
    this.config = config
    this.typeFields = this.type.typeSpec().fields
    this.queryParser = new QueryParser()
    this.paramN = 1
    this.params = []
    this.joinN = 1
    this.joins = {}
    this.symbolics = []
    this.symbolicParams = {}
  }
  /**
   * parse and generate filter
   * **NEW** and sort clauses (enables support for sort by joined fields)
   * **NEW** with join specs from both.
   * @param {*} filter
   * @param {*} sort
   */
  queryFilter(filter, sort = "") {
    this.paramN = 1
    this.params = []
    this.joinN = 1
    this.joins = {}
    let sortSpec = null
    this.symbolics = []
    let whereClause = null

    if (filter.trim().length > 0) {
      let root
      try {
        root = this.queryParser.parseFilter(filter)
      } catch (ex) {
        this.log.error(`Error parsing filter string: '${filter}'`)
        throw new Error(`Invalid filter: ${ex.message}`)
      }
      whereClause = this.transFilterNode(root)
    }

    sortSpec = this.querySort(sort)
    return {
      where: whereClause,
      params: this.params,
      joinSpecs: this.joins,
      sortSpec,
      symbolics: this.symbolics,
      symbolicParams: this.symbolicParams,
    }
  }

  transFilterNode(node) {
    switch (node.type) {
      case "andClause":
        return this.transFilterAnd(node)
        break

      case "orClause":
        return this.transFilterOr(node)
        break

      case "notExpression":
        return this.transFilterNot(node)
        break

      case "equalPredicate":
      case "notEqualPredicate":
      case "greaterEqualPredicate":
      case "greaterPredicate":
      case "lessEqualPredicate":
      case "lessPredicate":
      case "matchesPredicate":
      case "notMatchesPredicate":
        return this.transFilterPredicate(node)
        break

      case "existsPredicate":
      case "notExistsPredicate":
        return this.transFilterExistsPredicate(node)
        break

      case "inPredicate":
      case "notInPredicate":
        return this.transFilterInPredicate(node)
        break

      case "stringLiteral":
        throw new Error(`String literal not expected as terminal node`)
        break

      case "number":
        throw new Error(`Numeric literal not expected as terminal node`)
        break

      case "trueLiteral":
      case "falseLiteral":
        throw new Error(
          "boolean (true/false) literal not expected as terminal node"
        )

      default:
        throw new Error(`node type not recognized: ${node.type}`)
    }
  }

  transFilterAnd(node) {
    if (node.children.length < 2) {
      throw new Error(`AND expects two children`)
    }

    const arg1 = this.transFilterNode(node.children[0])
    const arg2 = this.transFilterNode(node.children[1])
    return `( ${arg1} AND ${arg2} )`
  }

  transFilterOr(node, tableAlias) {
    if (node.children.length < 2) {
      throw new Error(`OR expects two children`)
    }

    const arg1 = this.transFilterNode(node.children[0])
    const arg2 = this.transFilterNode(node.children[1])
    return `( ${arg1} OR ${arg2} )`
  }

  transFilterNot(node) {
    throw new Error("Not(), Not Currently Supported. Stay tuned!")
    return {}
  }

  transFilterPredicate(node) {
    let pred = ""
    const { fieldName, fieldParams } = this.getFieldReference(node.children)
    const columnName = this.getColumnName(
      fieldName,
      null,
      null,
      true,
      fieldParams
    )
    const literals = this.getLiterals(node.children)
    const rvalue = literals[0]

    switch (node.type) {
      case "equalPredicate":
        pred = `${columnName} = $${this.paramN}`
        break

      case "notEqualPredicate":
        pred = `${columnName} != $${this.paramN}`
        break

      case "greaterEqualPredicate":
        pred = `${columnName} >= $${this.paramN}`
        break

      case "greaterPredicate":
        pred = `${columnName} > $${this.paramN}`
        break

      case "lessEqualPredicate":
        pred = `${columnName} <= $${this.paramN}`
        break

      case "lessPredicate":
        pred = `${columnName} < $${this.paramN}`
        break

      case "matchesPredicate":
        pred = `${columnName} ~* $${this.paramN}`
        break
    }

    this.params.push(rvalue)
    this.paramN += 1
    return pred
  }

  transFilterExistsPredicate(node) {
    let pred = {}
    const { fieldName, fieldParams } = this.getFieldReference(node.children)
    const columnName = this.getColumnName(
      fieldName,
      null,
      null,
      true,
      fieldParams
    )

    switch (node.type) {
      case "existsPredicate":
        pred = `${columnName} IS NOT NULL `
        break

      case "notExistsPredicate":
        pred = `${columnName} IS NULL `
        break
    }

    return pred
  }
  /**
   * @param {*} node
   * @param {*} tableAlias
   */

  transFilterInPredicate(node) {
    let pred = {}
    const { fieldName, fieldParams } = this.getFieldReference(node.children)
    const columnName = this.getColumnName(
      fieldName,
      null,
      null,
      true,
      fieldParams
    )
    const literals = this.getLiterals(node.children)
    const ptokens = literals.map((l) => `$${this.paramN++}`)
    literals.forEach((l) => {
      this.params.push(l)
    })

    switch (node.type) {
      case "inPredicate":
        pred = `${columnName} IN ( ${ptokens.join(", ")} ) `
        break

      case "notInPredicate":
        pred = `${columnName} NOT IN ( ${ptokens.join(", ")} ) `
        break
    }

    return pred
  }

  getFieldReference(args) {
    const fields = args.filter((fitem) => fitem.type == "field")

    if (fields.length == 0) {
      console.log(`Error finding field: ${JSON.stringify(args, null, 2)}`)
      throw new Error("field not found")
    }

    const field = fields[0]
    let fieldParams = []
    const identifiers = field.children.filter(
      (item) => item.type == "identifier"
    )

    if (identifiers.length == 0) {
      throw new Error("Field identifier not found")
    }

    const fieldName = identifiers[0].value

    const paramList = field.children.filter((item) => item.type == "paramList")
    if (paramList.length) {
      fieldParams = this.getLiterals(paramList[0].children)
    }

    return { fieldName, fieldParams }
  }

  getColumnName(
    fieldName,
    parentTableAlias = null,
    parentFields = null,
    withAlias = true,
    symbolicParams = null
  ) {
    parentTableAlias = parentTableAlias || "t0"
    const { fieldSpec, tableAlias } = this.getFieldSpec(
      fieldName,
      parentTableAlias,
      parentFields
    )
    let prefix = ""

    if (withAlias) {
      prefix = tableAlias ? tableAlias + "." : ""
    }

    const colName = fieldSpec.asName ? fieldSpec.asName : fieldSpec.fieldName
    const qualifiedColName = prefix + colName

    if (fieldSpec.symbolic) {
      this.symbolics.push(qualifiedColName)
      this.symbolicParams[qualifiedColName] = symbolicParams
    }

    return qualifiedColName
  }
  /**
   * Get the typeSpec for the field named,
   * if it is compound ( nested with dot notation) get the typeSpec of the ultimate (last) field name
   *
   * @param {*} fieldName
   * @param {*} tableAlias
   * @param {*} typeFields
   */

  getFieldSpec(fieldName, tableAlias = "", typeFields = null) {
    typeFields = typeFields || this.typeFields
    const fieldNameParse = fieldName.split(".")
    let fieldSpec

    if (fieldNameParse.length > 1) {
      // field of a joined object
      let index = null
      const rootFieldName = fieldNameParse[0]
      const ix = parseInt(fieldNameParse[1])
      let subFieldName = null

      if (isNaN(ix)) {
        subFieldName = fieldNameParse.slice(1).join(".")
      } else {
        // an indexed query
        if (fieldNameParse.length < 3) {
          throw Error("indexed sub-field missing: ${fieldName}")
        }

        index = ix
        subFieldName = fieldNameParse.slice(2).join(".")
      }

      const res = this.getJoinedFieldSpec(
        rootFieldName,
        index,
        subFieldName,
        tableAlias,
        typeFields
      )
      fieldSpec = res.fieldSpec
      tableAlias = res.tableAlias
    } else {
      // direct field of the base object.
      if (typeFields.hasOwnProperty(fieldName)) {
        fieldSpec = typeFields[fieldName]
        fieldSpec.fieldName = fieldName //reiterate in case this is a joined field.
      } else {
        throw new Error(
          `Query Parser error: Field: '${fieldName}' does not exist`
        )
      }
    }

    return {
      fieldSpec,
      tableAlias,
    }
  }
  /**
   * Get the spec for the field which is a member of a another object field
   * This recurses indefinitely so can support field1.field2.field3
   * This also supports indexed field references object.1.field
   *
   * @param {*} parentFieldName
   * @param {*} index
   * @param {*} fieldName
   * @param {*} parentAlias
   * @param {*} grandParentFields
   */

  getJoinedFieldSpec(
    parentFieldName,
    index,
    fieldName,
    parentAlias,
    grandParentFields
  ) {
    let tableAlias = ""
    const parentFieldSpecRes = this.getFieldSpec(
      parentFieldName,
      parentAlias,
      grandParentFields
    )
    const parentFieldSpec = parentFieldSpecRes.fieldSpec

    if (parentFieldSpec.type != "ObjectRef") {
      throw new Error(`Field ${parentFieldName} is not a typeReference `)
    }

    let joinMode = "1"
    let parentType = parentFieldSpec.objectType

    if (parentFieldSpec.relationshipName) {
      joinMode = "X" // Intermediate M-M join "X"
    } else {
      if (parentFieldSpec.array) {
        joinMode = "M"
      }
    }

    if (joinMode == "1" && index != null) {
      throw Error("indexes can only be applied to Multi-valued fields")
    }

    const parentTypeSpec = this.typeMap[parentType]
    tableAlias = this.getJoinFieldAlias(
      parentFieldName,
      index,
      parentFieldSpec,
      parentType,
      parentAlias,
      joinMode,
      grandParentFields
    ) // Recursion point...

    const res = this.getFieldSpec(fieldName, tableAlias, parentTypeSpec.fields)
    return res
  }

  getJoinFieldAlias(
    fieldName,
    index,
    fieldSpec,
    joinTypeName,
    parentAlias,
    joinMode = "1",
    parentFields
  ) {
    let tableAlias = ""
    let xTableAlias = ""
    const joinKey = `${parentAlias}.${index != null ? index : ""}.${fieldName}`
    let joinSpec = this.joins[joinKey]

    if (joinSpec) {
      // already joined
      tableAlias = joinSpec.tableAlias
    } else {
      // add joinSpec
      tableAlias = "t" + this.joinN
      xTableAlias = "xt" + this.joinN // if needed for X joins

      this.joinN += 1
      const joinTypeSpec = this.typeMap[joinTypeName]

      if (!joinTypeSpec) {
        this.log.error(
          `Type not found in typeMap: ${joinTypeName}.  You may need to import this type from another Type module?`
        )
        throw new Error(
          `TypeName: ${joinTypeName} not found in typeMap. Import from another module required?`
        )
      }

      const joinTypeFields = joinTypeSpec.fields

      if (joinMode == "1") {
        if (index != null) {
          throw new Error(
            `Indexed field references for single joins not supported`
          )
        }

        let refFieldNames = fieldSpec.refField // check refField set
        if (!refFieldNames) {
          throw new Error(
            `join Field '${fieldName}' must include 'refField' property `
          )
        }
        // process generally allowing for 1 or more join field combinations.

        if (!Array.isArray(refFieldNames)) {
          refFieldNames = [refFieldNames]
        }

        let toRefFieldNames = fieldSpec.toRefField || "id"
        if (!Array.isArray(toRefFieldNames)) {
          toRefFieldNames = [toRefFieldNames]
        }

        if (refFieldNames.length != toRefFieldNames.length) {
          throw new Error(
            `refField and toRefField must have same number of entries. fieldName: ${fieldName}`
          )
        }

        let joinFromTableColumn = []
        let joinToTableColumn = []
        let onArray = []
        for (let i = 0; i < refFieldNames.length; i++) {
          const fromColumn = this.getColumnName(
            refFieldNames[i],
            parentAlias,
            parentFields
          )
          joinFromTableColumn.push(fromColumn)

          const toRefFieldName = toRefFieldNames[i]
          const joinToFieldSpec = joinTypeFields[toRefFieldName] // assume join to field named "id" by convention. Could look for "key" assuming single field key.
          const joinToColumn = joinToFieldSpec.asName
            ? joinToFieldSpec.asName
            : toRefFieldName
          const toColumn = `${tableAlias}.${joinToColumn}`

          joinToTableColumn.push(toColumn)

          onArray.push(`${fromColumn} = ${toColumn} `)
        }

        const onClause = onArray.join(" AND ")

        joinSpec = {
          tableAlias,
          typeName: joinTypeName,
          joinMode,
          onClause,
          joinFromColumn: joinFromTableColumn,
          idColumn: joinToTableColumn,
        }
      } else if (joinMode == "M") {
        const joinFromColumn = this.getColumnName(
          "id",
          parentAlias,
          parentFields
        )
        const toRefFieldName = fieldSpec.toRefField // check toRefField is set.

        if (!toRefFieldName) {
          throw new Error(
            `join Field '${fieldName}' must include 'toRefField' property`
          )
        }

        const joinToFieldSpec = joinTypeFields[toRefFieldName]
        const joinToColumn = joinToFieldSpec.asName
          ? joinToFieldSpec.asName
          : toRefFieldName
        const joinToIdSpec = joinTypeFields["id"]
        const joinToIdColumn = joinToIdSpec.asName ? joinToIdSpec.asName : "id"
        const idColumn = `${tableAlias}.${joinToIdColumn}`

        if (index != null) {
          // indexed multi-join filter
          const subQueryAlias = tableAlias + "j"
          const fieldSort = fieldSpec.sort

          if (!fieldSort) {
            throw new Error(
              `Multi-join fields require default 'sort' spec for indexed filters`
            )
          }

          const sortFieldName = fieldSort.field
          const sortDir = fieldSort.dir
          const sortFieldSpec = joinTypeFields[sortFieldName]
          const sortColumn = sortFieldSpec.asName
            ? sortFieldSpec.asName
            : sortFieldName // EX: `t1.claim_id = ( select t1j.claim_id  from claim_event_client t1j where t0.e_id = t1j.event_claimed_id order by t1j.claim_timestamp desc offset 0 limit 1 )`

          const onClause = `${tableAlias}.${joinToIdColumn} = ( SELECT ${subQueryAlias}.${joinToIdColumn}  FROM <<JOIN-TABLE>> ${subQueryAlias} WHERE ${joinFromColumn} = ${subQueryAlias}.${joinToColumn} ORDER BY ${subQueryAlias}.${sortColumn} ${sortDir} offset ${index} limit 1 )`
          joinSpec = {
            tableAlias,
            typeName: joinTypeName,
            joinMode: "MX",
            onClause,
            groupBy: joinFromColumn,
            // actually unnecessary?
            joinFromColumn,
            idColumn,
          }
        } else {
          // non-indexed (ANY) multi-join filter
          const onClause = `${joinFromColumn} = ${tableAlias}.${joinToColumn} `
          joinSpec = {
            tableAlias,
            typeName: joinTypeName,
            joinMode,
            onClause,
            groupBy: joinFromColumn,
            joinFromColumn,
            idColumn,
          }
        }
      } else if (joinMode == "X") {
        const refField = fieldSpec.refField || "id"
        const joinFromTableIdColumn = this.getColumnName(
          refField,
          parentAlias,
          parentFields
        )
        const toRefField = fieldSpec.toRefField || "id"
        const joinToFieldSpec = joinTypeFields[toRefField]
        const joinToIdColumn = joinToFieldSpec.asName
          ? joinToFieldSpec.asName
          : toRefField
        const joinToTableIdColumn = `${tableAlias}.${joinToIdColumn}`
        const relationshipName = fieldSpec.relationshipName

        if (!relationshipName) {
          throw new Error(`Field spec requires 'relationshipName' property.`)
        }

        if (index != null) {
          this.log.error(`Not yet supported X indexed`)
          throw new Error(`Intermediate indexed joins not supported. Yet!`)
        } else {
          const xOnClause = `${joinFromTableIdColumn} = ${xTableAlias}.<<X_JOIN_COLUMN>>`
          const onClause = `${joinToTableIdColumn} = ${xTableAlias}.<<X_KEY_COLUMN>>`
          joinSpec = {
            joinMode,
            tableAlias,
            xTableAlias,
            relationshipName,
            typeName: joinTypeName,
            onClause,
            xOnClause,
            joinFromColumn: joinFromTableIdColumn,
            idColumn: joinToTableIdColumn,
          }
        }
      } else {
        throw new Error(`Unknown join mode: ${joinMode}`)
      }

      this.joins[joinKey] = joinSpec
    }

    return tableAlias
  }

  getLiterals(args) {
    const literals = args
      .filter(
        (item) =>
          item.type == "stringLiteral" ||
          item.type == "number" ||
          item.type == "trueLiteral" ||
          item.type == "falseLiteral"
      )
      .map((item) => item.value)

    if (literals.length == 0) {
      throw new Error("literal not found")
    }

    return literals
  }

  getAscDesc(args) {
    const literals = args.filter(
      (item) => item.type == "asc" || item.type == "desc"
    )

    if (literals.length == 0) {
      return 1
    }

    return literals[0].type == "desc" ? -1 : 1
  }

  querySort(sort) {
    if (sort.trim().length > 0) {
      let root

      try {
        root = this.queryParser.parseSort(sort)

        if (root.type != "sortSpecList") {
          throw new Error(`Invalid sort`)
        }
      } catch (ex) {
        this.log.error(`Error parsing sort string: '${sort}'`)
        throw new Error(`Invalid sort: ${ex.message}`)
      }

      const sortList = root.children
      return this.transSortList(sortList)
    }

    return ""
  }

  transSortList(sortList) {
    let orderBy = []
    sortList.forEach((node) => {
      const { columnName, dir } = this.transSort(node)
      const sqlDir = dir == 1 ? "ASC" : "DESC"
      orderBy.push(`${columnName} ${sqlDir}`)
    })
    return orderBy.join(", ")
  }

  transSort(node) {
    const { fieldName, fieldParams } = this.getFieldReference(node.children)
    const columnName = this.getColumnName(
      fieldName,
      null,
      null,
      true,
      fieldParams
    )
    const dir = this.getAscDesc(node.children)
    return {
      columnName,
      dir,
    }
  }
}
