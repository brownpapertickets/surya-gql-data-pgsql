import { BaseType } from "@brownpapertickets/surya-gql-types"
import { ScalarTypes } from "@brownpapertickets/surya-gql-scalar"

export class EventTestSet extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  typeSpec() {
    return {
      name: "EventTestSet",
      extends: null,
      instantiable: true,
      timestamps: true,
      permissions: {
        Secure: ["admin"],
      },
      fields: {
        id: {
          type: ScalarTypes.ID,
          asName: "etsetr_id",
          key: true,
          required: true,
          input: false,
        },
        eventId: {
          type: ScalarTypes.Integer,
          asName: "etsetr_event_id",
          required: true,
        },
        timestamp: {
          type: ScalarTypes.DateTime,
          asName: "etsetr_timestamp",
        },
        requestClientId: {
          type: ScalarTypes.Integer,
          asName: "etsetr_request_client_id",
        },
        result: {
          type: ScalarTypes.String,
          asName: "etsetr_result",
        },
        rollupResult: {
          type: ScalarTypes.Enum,
          enum: ["fail", "pass", "warn"],
          symbolic: true,
          input: false,
        },
        testResults: {
          type: ScalarTypes.ObjectRef,
          objectType: "EventTestResult",
          params: "query",
          refField: "id",
          refFieldType: ScalarTypes.Integer,
          inputs: ["EventTestResultInput"],
          array: true,
        },
      },
    }
  }
}
