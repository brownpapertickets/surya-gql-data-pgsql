import { BaseType } from "@brownpapertickets/surya-gql-types"
import { ScalarTypes } from "@brownpapertickets/surya-gql-scalar"

export class Client extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  typeSpec() {
    return {
      name: "Client",
      extends: null,
      instantiable: true,
      timestamps: true,
      permissions: {
        Secure: ["admin"],
      },
      fields: {
        id: {
          type: ScalarTypes.ID,
          asName: "c_id",
          key: true,
          required: true,
          input: false,
        },
        clientName: {
          type: ScalarTypes.String,
          asName: "client_name",
          required: true,
        },
        firstName: {
          type: ScalarTypes.String,
          asName: "fname",
        },
        lastName: {
          type: ScalarTypes.String,
          asName: "lname",
        },
        nameForChecks: {
          type: ScalarTypes.String,
          asName: "name_for_checks",
        },
        ssno: {
          type: ScalarTypes.String,
          asName: "ss",
        },
        notes: {
          type: ScalarTypes.String,
          asName: "notes",
        },
        address: {
          type: ScalarTypes.String,
          asName: "address",
        },
        city: {
          type: ScalarTypes.String,
          asName: "city",
        },
        state: {
          type: ScalarTypes.String,
          asName: "state",
        },
        zip: {
          type: ScalarTypes.String,
          asName: "zip",
        },
        country: {
          type: ScalarTypes.String,
          asName: "country",
        },
        phone: {
          type: ScalarTypes.String,
          asName: "phone",
        },
        email: {
          type: ScalarTypes.String,
          asName: "email",
        },
        lastVisit: {
          type: ScalarTypes.DateTime,
          asName: "last_visit",
        },
        registrationDate: {
          type: ScalarTypes.DateTime,
          asName: "registration_date",
        },
        bannedDate: {
          type: ScalarTypes.DateTime,
          asName: "banned_date",
        },
        registrationIp: {
          type: ScalarTypes.String,
          asName: "registration_ip",
        },
        banned: {
          type: ScalarTypes.String,
          asName: "banned",
        },
        bannedNotes: {
          type: ScalarTypes.String,
          asName: "banning_note",
        },
        locked: {
          type: ScalarTypes.Boolean,
          asName: "lockout",
        },
      },
    }
  }
}
