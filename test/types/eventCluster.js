import { BaseType } from "@brownpapertickets/surya-gql-types"
import { ScalarTypes } from "@brownpapertickets/surya-gql-scalar"
import { Category } from "./category"

export class EventCluster extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  childTypes() {
    return {
      Category,
    }
  }

  typeSpec() {
    return {
      name: "EventCluster",
      extends: null,
      instantiable: true,
      timestamps: true,
      permissions: {
        secure: ["ADMINISTRATOR"],
      },
      fields: {
        id: {
          type: ScalarTypes.ID,
          asName: "cluster_id",
          key: true,
          required: true,
          input: false,
        },
        name: {
          type: ScalarTypes.String,
          asName: "cluster_name",
        },
        description: {
          type: ScalarTypes.String,
          asName: "cluster_description",
        },
        ownerId: {
          type: ScalarTypes.Integer,
          asName: "c_id",
        },
        public: {
          type: ScalarTypes.Boolean,
          asName: "c_public",
        },
        producerPage: {
          type: ScalarTypes.Boolean,
          asName: "c_producer_page",
        },
        allowTop10: {
          type: ScalarTypes.Boolean,
          asName: "c_allowtop10",
        },
        forceTop10: {
          type: ScalarTypes.Boolean,
          asName: "c_forcetop10",
        },
        doNotChange: {
          type: ScalarTypes.Boolean,
          asName: "c_donotchange",
        },
        owner: {
          type: ScalarTypes.ObjectRef,
          objectType: "Client",
          refField: "ownerId",
          refFieldType: ScalarTypes.Integer,
          input: false,
        },
        categories: {
          type: ScalarTypes.ObjectRef,
          objectType: "Category",
          array: true,
          refField: "id",
          refFieldType: ScalarTypes.Integer,
          relationshipName: "eventClusterCategories",
          input: false,
        },
      },
    }
  }
}
