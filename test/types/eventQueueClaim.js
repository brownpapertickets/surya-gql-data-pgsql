import { BaseType } from "@brownpapertickets/surya-gql-types"
import { ScalarTypes } from "@brownpapertickets/surya-gql-scalar"

export class EventQueueClaim extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  typeSpec() {
    return {
      name: "EventQueueClaim",
      extends: null,
      instantiable: true,
      timestamps: true,
      permissions: {
        Secure: ["admin"],
      },
      fields: {
        id: {
          type: ScalarTypes.ID,
          asName: "claim_id",
          key: true,
          required: true,
          input: false,
        },
        eventId: {
          type: ScalarTypes.Integer,
          asName: "event_claimed_id",
        },
        clientId: {
          type: ScalarTypes.Integer,
          asName: "claim_client_id",
        },
        claimTimestamp: {
          type: ScalarTypes.DateTime,
          asName: "claim_timestamp",
        },
        unclaimTimestamp: {
          type: ScalarTypes.DateTime,
          asName: "unclaim_timestamp",
        },
        client: {
          type: ScalarTypes.ObjectRef,
          objectType: "Client",
          refField: "clientId",
          refFieldType: ScalarTypes.Integer,
          input: false,
        },
      },
    }
  }
}
